module gitee.com/changeden/jd-union-go-sdk

go 1.16

require (
	github.com/guangzhou-meta/go-lib/strings v0.0.0-20220113062126-84df1b3c3b5c
	github.com/guangzhou-meta/go-lib/tools v0.0.0-20220113062126-84df1b3c3b5c
	github.com/stretchr/testify v1.7.0
)
