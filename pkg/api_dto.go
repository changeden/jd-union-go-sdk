package pkg

import (
	"encoding/json"
	"fmt"
)

type ApiResponseDTO struct {
	Error *ErrorResponse `json:"error_response"`

	Result *ResultResponse `json:"result"`
}

func (d *ApiResponseDTO) IsError() bool {
	return d.Error != nil
}

func (d *ApiResponseDTO) Unmarshal(resp interface{}) error {
	if d.IsError() || d.Result == nil {
		return fmt.Errorf(d.Error.ZHDesc)
	}
	return json.Unmarshal([]byte(d.Result.QueryResult), resp)
}

type ErrorResponse struct {
	Code   string `json:"code"`
	ZHDesc string `json:"zh_desc"`
	ENDesc string `json:"en_desc"`
}

type ResultResponse struct {
	Code        string `json:"code"`
	QueryResult string `json:"queryResult"`
}

type QueryResultBase struct {
	Code      int    `json:"code"`
	Message   string `json:"message"`
	RequestId string `json:"requestId"`
}
