package pkg

type ApiMethodPlugin interface {
	GetRequest() func(method *ApiMethod, params ApiRequestBody, accessToken *string) (*ApiResponseDTO, error)
}

type ApiMethodPluginBase struct {
	Request func(method *ApiMethod, params ApiRequestBody, accessToken *string) (*ApiResponseDTO, error)
}

func (m ApiMethodPluginBase) GetRequest() func(method *ApiMethod, params ApiRequestBody, accessToken *string) (*ApiResponseDTO, error) {
	return m.Request
}

type ApiRequestBody interface {
	GetDTOName() string
}
