package pkg

type ApiMethod struct {
	Method          string
	ResponseWrapKey string
}

func NewApiMethod(method string, responseWrapKey string) *ApiMethod {
	return &ApiMethod{
		Method:          method,
		ResponseWrapKey: responseWrapKey,
	}
}

// 推广物料
var (
	JdUnionOpenGoodsJingfenQuery               = NewApiMethod("jd.union.open.goods.jingfen.query", "jd_union_open_goods_jingfen_query_responce")
	JdUnionOpenGoodsQuery                      = NewApiMethod("jd.union.open.goods.query", "jd_union_open_goods_query_response")
	JdUnionOpenGoodsMaterialQuery              = NewApiMethod("jd.union.open.goods.material.query", "jd_union_open_goods_material_query_response")
	JdUnionOpenGoodsPromotiongoodsinfoQuery    = NewApiMethod("jd.union.open.goods.promotiongoodsinfo.query", "jd_union_open_goods_promotiongoodsinfo_query_response")
	JdUnionOpenCategoryGoodsGet                = NewApiMethod("jd.union.open.category.goods.get", "jd_union_open_category_goods_get_response")
	JdUnionOpenGoodsBigfieldQuery              = NewApiMethod("jd.union.open.goods.bigfield.query", "jd_union_open_goods_bigfield_query_response")
	JdUnionOpenCouponQuery                     = NewApiMethod("jd.union.open.coupon.query", "jd_union_open_coupon_query_response")
	JdUnionOpenActivityQuery                   = NewApiMethod("jd.union.open.activity.query", "jd_union_open_activity_query_response")
	JdUnionOpenActivityRecommendQuery          = NewApiMethod("jd.union.open.activity.recommend.query", "jd_union_open_activity_recommend_query_response")
	JdUnionOpenPromotionIntelligenceQuery      = NewApiMethod("jd.union.open.promotion.intelligence.query", "jd_union_open_promotion_intelligence_query_response")
	JdUnionOpenPromotionToolsIntelligenceQuery = NewApiMethod("jd.union.open.promotion.tools.intelligence.query", "jd_union_open_promotion_tools_intelligence_query_response")
)

// 转链能力
var (
	JdUnionOpenPromotionCommonGet       = NewApiMethod("jd.union.open.promotion.common.get", "jd_union_open_promotion_common_get_response")
	JdUnionOpenPromotionBysubunionidGet = NewApiMethod("jd.union.open.promotion.bysubunionid.get", "jd_union_open_promotion_bysubunionid_get_response")
	JdUnionOpenPromotionByunionidGet    = NewApiMethod("jd.union.open.promotion.byunionid.get", "jd_union_open_promotion_byunionid_get_response")
)

// 推广效果
var (
	JdUnionOpenOrderRowQuery                 = NewApiMethod("jd.union.open.order.row.query", "jd_union_open_order_row_query_response")
	JdUnionOpenOrderBonusQuery               = NewApiMethod("jd.union.open.order.bonus.query", "jd_union_open_order_bonus_query_response")
	JdUnionOpenStatisticsRedpacketQuery      = NewApiMethod("jd.union.open.statistics.redpacket.query", "jd_union_open_statistics_redpacket_query_response")
	JdUnionOpenOrderAgentQuery               = NewApiMethod("jd.union.open.order.agent.query", "jd_union_open_order_agent_query_response")
	JdUnionOpenStatisticsRedpacketAgentQuery = NewApiMethod("jd.union.open.statistics.redpacket.agent.query", "jd_union_open_statistics_redpacket_agent_query_response")
	JdUnionOpenActivityBonusQuery            = NewApiMethod("jd.union.open.activity.bonus.query", "jd_union_open_activity_bonus_query_response")
	JdUnionOpenStatisticsActivityBonusQuery  = NewApiMethod("jd.union.open.statistics.activity.bonus.query", "jd_union_open_statistics_activity_bonus_query_response")
)

// 营销工具
var (
	JdUnionOpenCouponGiftGet             = NewApiMethod("jd.union.open.coupon.gift.get", "jd_union_open_coupon_gift_get_response")
	JdUnionOpenCouponGiftStop            = NewApiMethod("jd.union.open.coupon.gift.stop", "jd_union_open_coupon_gift_stop_response")
	JdUnionOpenStatisticsGiftcouponQuery = NewApiMethod("jd.union.open.statistics.giftcoupon.query", "jd_union_open_statistics_giftcoupon_query_response")
	JdUnionOpenUserRegisterValidate      = NewApiMethod("jd.union.open.user.register.validate", "jd_union_open_user_register_validate_response")
)

// 管理工具
var (
	JdUnionOpenPositionCreate       = NewApiMethod("jd.union.open.position.create", "jd_union_open_position_create_response")
	JdUnionOpenPositionQuery        = NewApiMethod("jd.union.open.position.query", "jd_union_open_position_query_response")
	JdUnionOpenUserPidGet           = NewApiMethod("jd.union.open.user.pid.get", "jd_union_open_user_pid_get_response")
	JdUnionOpenChannelInvitecodeGet = NewApiMethod("jd.union.open.channel.invitecode.get", "jd_union_open_channel_invitecode_get_response")
	JdUnionOpenChannelRelationGet   = NewApiMethod("jd.union.open.channel.relation.get", "jd_union_open_channel_relation_get_response")
	JdUnionOpenChannelRelationQuery = NewApiMethod("jd.union.open.channel.relation.query", "jd_union_open_channel_relation_query_response")
)

// 商羚
var (
	JdUnionOpenSellingGoodsQuery    = NewApiMethod("jd.union.open.selling.goods.query", "jd_union_open_selling_goods_query_response")
	JdUnionOpenSellingPromotionGet  = NewApiMethod("jd.union.open.selling.promotion.get", "jd_union_open_selling_promotion_get_response")
	JdUnionOpenSellingOrderRowQuery = NewApiMethod("jd.union.open.selling.order.row.query", "jd_union_open_selling_order_row_query_response")
)
