package test

import (
	"testing"
)

import (
	"github.com/stretchr/testify/assert"
)

import (
	api "gitee.com/changeden/jd-union-go-sdk"
	"gitee.com/changeden/jd-union-go-sdk/test/pkg"
)

const (
	appKey    = "eefc33bDRea044cb8ctre5hycf0ac1934" // 非真实信息
	appSecret = "6d34r0d0kild46460654b42f5e350982"  // 非真实信息
)

func TestApiConfig(t *testing.T) {
	apiConfig := api.NewApiConfig(appKey, appSecret)
	apis, err := apiConfig.Build()
	assert.Nil(t, err)
	eliteId := 22
	reqDTO := pkg.NewJdUnionOpenGoodsJingfenQueryRequest()
	reqDTO["eliteId"] = eliteId
	accessToken := "1"
	res, err := apis.JdUnionOpenGoodsJingfenQuery(&reqDTO, &accessToken)
	assert.Nil(t, err)
	assert.True(t, res.IsError())
	assert.Nil(t, res.Result)
}

func TestSign(t *testing.T) {
	apiConfig := api.NewApiConfig(appKey, appSecret)
	apiBody := api.NewApiBody(apiConfig)
	eliteId := 22
	reqDTO := pkg.NewJdUnionOpenGoodsJingfenQueryRequest()
	reqDTO["eliteId"] = eliteId
	apiBody.SetParamJson(&reqDTO)
	accessToken := "1"
	apiBody.SetAccessToken(&accessToken)
	sign, err := api.Sign(apiBody, appSecret)
	assert.Nil(t, err)
	assert.NotNil(t, sign)
}
