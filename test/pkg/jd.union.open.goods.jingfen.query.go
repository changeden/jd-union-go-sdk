package pkg

type JdUnionOpenGoodsJingfenQueryRequestDTO map[string]interface{}

func NewJdUnionOpenGoodsJingfenQueryRequest() JdUnionOpenGoodsJingfenQueryRequestDTO {
	return JdUnionOpenGoodsJingfenQueryRequestDTO{}
}

func (d *JdUnionOpenGoodsJingfenQueryRequestDTO) GetDTOName() string {
	return "goodsReq"
}
