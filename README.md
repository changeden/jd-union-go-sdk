## jd-union

> 京东联盟Go SDK
>
> [全部API](https://union.jd.com/openplatform/api/v2)

### 安装

```shell
go get -u gitee.com/changeden/jd-union-go-sdk
```

### 初始化

```go
package main

import (
	"log"
)

import (
	api "gitee.com/changeden/jd-union-go-sdk"
)

func main() {
	apiConfig := api.NewApiConfig("<联盟分配给应用的appKey>", "<联盟分配给应用的appSecret>")
	apis, err := apiConfig.Build()
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(apis)
}
```

### 调用API

```go
package main

import (
	"log"
)

import (
	api "gitee.com/changeden/jd-union-go-sdk"
)

// 声明请求体DTO
type JdUnionOpenGoodsJingfenQueryRequestDTO map[string]interface{}

func NewJdUnionOpenGoodsJingfenQueryRequest() JdUnionOpenGoodsJingfenQueryRequestDTO {
	return JdUnionOpenGoodsJingfenQueryRequestDTO{}
}

// 实现pkg.ApiRequestBody接口
func (d *JdUnionOpenGoodsJingfenQueryRequestDTO) GetDTOName() string {
	return "goodsReq"
}

// 声明响应体DTO
type JdUnionOpenGoodsJingfenQueryResponseDTO map[string]interface{}

func main() {
	apiConfig := api.NewApiConfig("<联盟分配给应用的appKey>", "<联盟分配给应用的appSecret>")
	apis, err := apiConfig.Build()
	if err != nil {
		log.Println(err)
		return
	}

	eliteId := 22
	reqDTO := NewJdUnionOpenGoodsJingfenQueryRequest()
	reqDTO["eliteId"] = eliteId

	var accessToken *string
	accessToken = nil
	res, err := apis.JdUnionOpenGoodsJingfenQuery(&reqDTO, accessToken)
	if err != nil {
		log.Println(err)
		return
	}
	if res.IsError() {
		log.Println(res.Error)
		return
	}
	log.Println(res.Result)

	var resp JdUnionOpenGoodsJingfenQueryResponseDTO
	err = res.Unmarshal(&resp)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(resp)
}
```

### 单独签名

```go
package main

import (
	"log"
)

import (
	api "gitee.com/changeden/jd-union-go-sdk"
)

// 声明请求体DTO
type JdUnionOpenGoodsJingfenQueryRequestDTO map[string]interface{}

func NewJdUnionOpenGoodsJingfenQueryRequest() JdUnionOpenGoodsJingfenQueryRequestDTO {
	return JdUnionOpenGoodsJingfenQueryRequestDTO{}
}

// 实现pkg.ApiRequestBody接口
func (d *JdUnionOpenGoodsJingfenQueryRequestDTO) GetDTOName() string {
	return "goodsReq"
}

func main() {
	apiConfig := api.NewApiConfig("<联盟分配给应用的appKey>", "<联盟分配给应用的appSecret>")
	apiBody := api.NewApiBody(apiConfig)

	eliteId := 22
	reqDTO := NewJdUnionOpenGoodsJingfenQueryRequest()
	reqDTO["eliteId"] = eliteId
	apiBody.SetParamJson(&reqDTO)

	accessToken := "1"
	apiBody.SetAccessToken(&accessToken)

	sign, err := api.Sign(apiBody, "<联盟分配给应用的appSecret>")
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(sign)
}
```

### API列表

API风格统一为官方method名的驼峰命名

#### 推广物料

|        名称        |                       method                       |                   SDK Api                   |
|:----------------:|:--------------------------------------------------:|:-------------------------------------------:| 
|    京粉精选商品查询接口    |         jd.union.open.goods.jingfen.query          |        JdUnionOpenGoodsJingfenQuery         |
|  关键词商品查询接口【申请】   |             jd.union.open.goods.query              |            JdUnionOpenGoodsQuery            |
|     猜你喜欢商品推荐     |         jd.union.open.goods.material.query         |        JdUnionOpenGoodsMaterialQuery        |
| 根据skuid查询商品信息接口  |    jd.union.open.goods.promotiongoodsinfo.query    |   JdUnionOpenGoodsPromotiongoodsinfoQuery   |
|     商品类目查询接口     |          jd.union.open.category.goods.get          |         JdUnionOpenCategoryGoodsGet         |
|     商品详情查询接口     |         jd.union.open.goods.bigfield.query         |        JdUnionOpenGoodsBigfieldQuery        |
| 优惠券领取情况查询接口【申请】  |             jd.union.open.coupon.query             |           JdUnionOpenCouponQuery            |
|      活动查询接口      |            jd.union.open.activity.query            |          JdUnionOpenActivityQuery           |
|    活动推荐接口【申请】    |       jd.union.open.activity.recommend.query       |      JdUnionOpenActivityRecommendQuery      |
|     线报推广【申请】     |     jd.union.open.promotion.intelligence.query     |    JdUnionOpenPromotionIntelligenceQuery    |
|   工具商线报推广【申请】    |  jd.union.open.promotion.tools.intelligence.query  | JdUnionOpenPromotionToolsIntelligenceQuery  |

#### 转链能力

|        名称        |                  method                   |               SDK Api               |
|:----------------:|:-----------------------------------------:|:-----------------------------------:| 
|  网站/APP获取推广链接接口  |    jd.union.open.promotion.common.get     |    JdUnionOpenPromotionCommonGet    |
| 社交媒体获取推广链接接口【申请】 | jd.union.open.promotion.bysubunionid.get  | JdUnionOpenPromotionBysubunionidGet |
| 工具商获取推广链接接口【申请】  |   jd.union.open.promotion.byunionid.get   |  JdUnionOpenPromotionByunionidGet   |

#### 推广效果

|         名称          |                     method                     |                 SDK Api                  |
|:-------------------:|:----------------------------------------------:|:----------------------------------------:| 
|       订单行查询接口       |         jd.union.open.order.row.query          |         JdUnionOpenOrderRowQuery         |
|    奖励订单查询接口【申请】     |        jd.union.open.order.bonus.query         |        JdUnionOpenOrderBonusQuery        |
|      京享红包效果数据       |    jd.union.open.statistics.redpacket.query    |   JdUnionOpenStatisticsRedpacketQuery    |
|   工具商订单行查询接口【申请】    |        jd.union.open.order.agent.query         |        JdUnionOpenOrderAgentQuery        |
| 工具商京享红包效果数据查询接口【申请】 | jd.union.open.statistics.redpacket.agent.query | JdUnionOpenStatisticsRedpacketAgentQuery |
|   奖励活动信息查询接口【申请】    |       jd.union.open.activity.bonus.query       |      JdUnionOpenActivityBonusQuery       |
|  奖励活动奖励金额查询接口【申请】   | jd.union.open.statistics.activity.bonus.query  | JdUnionOpenStatisticsActivityBonusQuery  |

#### 营销工具

|       名称       |                  method                   |               SDK Api                |
|:--------------:|:-----------------------------------------:|:------------------------------------:| 
|      礼金创建      |       jd.union.open.coupon.gift.get       |       JdUnionOpenCouponGiftGet       |
|      礼金停止      |      jd.union.open.coupon.gift.stop       |      JdUnionOpenCouponGiftStop       |
|     礼金效果数据     | jd.union.open.statistics.giftcoupon.query | JdUnionOpenStatisticsGiftcouponQuery |
| 京东注册用户判定接口【申请】 |   jd.union.open.user.register.validate    |   JdUnionOpenUserRegisterValidate    |

#### 管理工具

|       名称       |                method                |             SDK Api             |
|:--------------:|:------------------------------------:|:-------------------------------:| 
|   创建推广位【申请】    |    jd.union.open.position.create     |    JdUnionOpenPositionCreate    |
|   查询推广位【申请】    |     jd.union.open.position.query     |    JdUnionOpenPositionQuery     |
|   获取PID【申请】    |      jd.union.open.user.pid.get      |      JdUnionOpenUserPidGet      |
|  邀请码获取接口【申请】   | jd.union.open.channel.invitecode.get | JdUnionOpenChannelInvitecodeGet |
| 渠道关系ID生成接口【申请】 |  jd.union.open.channel.relation.get  |  JdUnionOpenChannelRelationGet  |
|  渠道关系查询接口【申请】  | jd.union.open.channel.relation.query | JdUnionOpenChannelRelationQuery |

#### 商羚

|   名称    |                method                 |             SDK Api             |
|:-------:|:-------------------------------------:|:-------------------------------:| 
| 商羚商品查询  |   jd.union.open.selling.goods.query   |  JdUnionOpenSellingGoodsQuery   |
| 商羚转链获取  |  jd.union.open.selling.promotion.get  | JdUnionOpenSellingPromotionGet  |
| 商羚订单行查询 | jd.union.open.selling.order.row.query | JdUnionOpenSellingOrderRowQuery |

### 其他SDK

[NodeJS](https://gitee.com/changeden/jd-union-nodejs-sdk)

### 更多计划

- [x] 提供Api请求方式
- [x] 提供单独签名方式
- [ ] 提供Api请求体DTO
- [ ] 提供Api响应体DTO
