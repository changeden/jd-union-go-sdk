package method

import (
	"gitee.com/changeden/jd-union-go-sdk/pkg"
)

// 推广物料

type ApiGoodsMethods struct {
	pkg.ApiMethodPluginBase
}

func (m *ApiGoodsMethods) JdUnionOpenGoodsJingfenQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenGoodsJingfenQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenGoodsQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenGoodsQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenGoodsMaterialQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenGoodsMaterialQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenGoodsPromotiongoodsinfoQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenGoodsPromotiongoodsinfoQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenCategoryGoodsGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenCategoryGoodsGet, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenGoodsBigfieldQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenGoodsBigfieldQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenCouponQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenCouponQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenActivityQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenActivityQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenActivityRecommendQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenActivityRecommendQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenPromotionIntelligenceQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenPromotionIntelligenceQuery, reqDTO, accessToken)
}

func (m *ApiGoodsMethods) JdUnionOpenPromotionToolsIntelligenceQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenPromotionToolsIntelligenceQuery, reqDTO, accessToken)
}
