package method

import (
	"gitee.com/changeden/jd-union-go-sdk/pkg"
)

// 推广效果
type ApiEffectMethods struct {
	pkg.ApiMethodPluginBase
}

func (m *ApiEffectMethods) JdUnionOpenOrderRowQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenOrderRowQuery, reqDTO, accessToken)
}

func (m *ApiEffectMethods) JdUnionOpenOrderBonusQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenOrderBonusQuery, reqDTO, accessToken)
}

func (m *ApiEffectMethods) JdUnionOpenStatisticsRedpacketQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenStatisticsRedpacketQuery, reqDTO, accessToken)
}

func (m *ApiEffectMethods) JdUnionOpenOrderAgentQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenOrderAgentQuery, reqDTO, accessToken)
}

func (m *ApiEffectMethods) JdUnionOpenStatisticsRedpacketAgentQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenStatisticsRedpacketAgentQuery, reqDTO, accessToken)
}

func (m *ApiEffectMethods) JdUnionOpenActivityBonusQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenActivityBonusQuery, reqDTO, accessToken)
}

func (m *ApiEffectMethods) JdUnionOpenStatisticsActivityBonusQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenStatisticsActivityBonusQuery, reqDTO, accessToken)
}
