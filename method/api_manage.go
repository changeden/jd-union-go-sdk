package method

import (
	"gitee.com/changeden/jd-union-go-sdk/pkg"
)

// 管理工具
type ApiManageMethods struct {
	pkg.ApiMethodPluginBase
}

func (m *ApiManageMethods) JdUnionOpenPositionCreate(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenPositionCreate, reqDTO, accessToken)
}

func (m *ApiManageMethods) JdUnionOpenPositionQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenPositionQuery, reqDTO, accessToken)
}

func (m *ApiManageMethods) JdUnionOpenUserPidGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenUserPidGet, reqDTO, accessToken)
}

func (m *ApiManageMethods) JdUnionOpenChannelInvitecodeGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenChannelInvitecodeGet, reqDTO, accessToken)
}

func (m *ApiManageMethods) JdUnionOpenChannelRelationGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenChannelRelationGet, reqDTO, accessToken)
}

func (m *ApiManageMethods) JdUnionOpenChannelRelationQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenChannelRelationQuery, reqDTO, accessToken)
}
