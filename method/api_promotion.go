package method

import (
	"gitee.com/changeden/jd-union-go-sdk/pkg"
)

// 转链能力
type ApiPromotionMethods struct {
	pkg.ApiMethodPluginBase
}

func (m *ApiPromotionMethods) JdUnionOpenPromotionCommonGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenPromotionCommonGet, reqDTO, accessToken)
}

func (m *ApiPromotionMethods) JdUnionOpenPromotionBysubunionidGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenPromotionBysubunionidGet, reqDTO, accessToken)
}

func (m *ApiPromotionMethods) JdUnionOpenPromotionByunionidGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenPromotionByunionidGet, reqDTO, accessToken)
}
