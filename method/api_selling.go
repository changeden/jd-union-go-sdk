package method

import (
	"gitee.com/changeden/jd-union-go-sdk/pkg"
)

// 商羚
type ApiSellingMethods struct {
	pkg.ApiMethodPluginBase
}

func (m *ApiSellingMethods) JdUnionOpenSellingGoodsQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenSellingGoodsQuery, reqDTO, accessToken)
}

func (m *ApiSellingMethods) JdUnionOpenSellingPromotionGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenSellingPromotionGet, reqDTO, accessToken)
}

func (m *ApiSellingMethods) JdUnionOpenSellingOrderRowQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenSellingOrderRowQuery, reqDTO, accessToken)
}
