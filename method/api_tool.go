package method

import (
	"gitee.com/changeden/jd-union-go-sdk/pkg"
)

// 营销工具
type ApiToolMethods struct {
	pkg.ApiMethodPluginBase
}

func (m *ApiToolMethods) JdUnionOpenCouponGiftGet(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenCouponGiftGet, reqDTO, accessToken)
}

func (m *ApiToolMethods) JdUnionOpenCouponGiftStop(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenCouponGiftStop, reqDTO, accessToken)
}

func (m *ApiToolMethods) JdUnionOpenStatisticsGiftcouponQuery(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenStatisticsGiftcouponQuery, reqDTO, accessToken)
}

func (m *ApiToolMethods) JdUnionOpenUserRegisterValidate(reqDTO pkg.ApiRequestBody, accessToken *string) (*pkg.ApiResponseDTO, error) {
	return m.GetRequest()(pkg.JdUnionOpenUserRegisterValidate, reqDTO, accessToken)
}
