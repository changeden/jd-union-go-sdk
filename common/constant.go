package common

const (
	DefaultServer = "https://api.jd.com/routerjson"

	DefaultFormat      = "json"
	DefaultVersion     = "1.0"
	DefaultSignMethod  = "md5"
	DefaultAccessToken = "undefined"
	DefaultParamJson   = "{}"
)

const (
	RequestMethod = "GET"

	ApiMethodPluginBaseRequestKey = "Request"

	ApiResponseErrorMsgKey    = "error_response"
	ApiResponseCodeKey        = "code"
	ApiResponseErrorZHDescKey = "zh_desc"
	ApiResponseErrorENDescKey = "en_desc"
	ApiResponseResultKey      = "queryResult"
)
